# gitops-node-deployments

Part of a solution for sportening task.

The repo is structured in three parts.

kubernetes/apps directory contains manifests for first part of the second assignment.

Helm directory contains the same resources as kubernetes directory, just done by using Helm.

kubernetes/argocd directory contains application definitions needed for the second part of the second assignment.
